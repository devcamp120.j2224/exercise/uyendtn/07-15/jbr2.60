import java.net.Socket;

public class App {
    public static void main(String[] args) throws Exception {
        //6. tạo 2 con animal
        Animal animal1 = new Animal("animalA");
        Animal animal2 = new Animal("animalB");
        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        //7.tạo 2 con mammal
        Mammal mammal1 = new Mammal("MammalA");
        Mammal mammal2 = new Mammal("MammalB");
        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        //8.tạo 2 con cat
        Cat cat1 = new Cat("CatA");
        Cat cat2 = new Cat("CatB");
        System.out.println(cat1.toString());
        System.out.println(cat2.toString());

        //9.tạo 2 con dog
        Dog dog1 = new Dog("DogA");
        Dog dog2 = new Dog("DogB");
        System.out.println(dog1.toString());
        System.out.println(dog2.toString());

        //10. in tiếng kêu 2 con cat
        cat1.greets();
        cat2.greets();

        //11.in tiếng kêu 2 con dog
        dog1.greets();
        dog2.greets();

        //12. in tiếng kêu của con dog 1 đối với con dog 2
        dog1.greets(dog2);
    }
}
