public class Dog extends Mammal{

    public Dog(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return "Dog [" + super.toString() + "]";
    }
    
    public void greets() {
        System.out.println("woof");
    }
    public void greets(Dog anotherDog) {
        System.out.println("woofwoof");
    }
}
